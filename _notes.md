# Initial Thoughts on Libraries for Recommender Systems

## Guiding Decisions to Help Choose
- What methods/techniques we decide we are going to want to implement or trial out
- If there are deep-learning requirements or at least options we want to have
- The bandwidth/time we have/want to give to learning new paradigms of code-execution and interfaces
- If we want the simplest, most stable and quick to get up and running with interface and are happy to forego more customisability
- What the databricks platform is compatible with, and whether it permits setting up virtual environments etc.

## Libraries Looked At

*issue with all of these out-of-the-box ones save tf-recommenders is that they are barely maintained, all having like 2 years since last update on github, which is strange considering recommendation is such a big area of application, perhaps people mostly build their own custom ones in dl/numeric-python libraries*

**out-of-the-box 'complete' libraries**
- [`surprise`](https://github.com/NicolasHug/Surprise)
  - simplest, easiest to understand interface
  - most traditional methods (linear algebra style) as in textbooks and blogs
  - seems stable w. large user base
  - seems fairly fast
  - not a deep learning library and unlikely to mix-in with them feasibly
  - doubtful as to customisation to enable more novel architectures and piping together different components

- [`lightfm`](https://github.com/lyst/lightfm)
  - a bit terse and obfuscating interface
  - not entirely obvious what options it has, seems sgd based only
  - not a deep learning library, but thats okay
  - stability is maybe ok, not as good as surprise it seems

- [`spotlight`](https://github.com/maciejkula/spotlight)
  - a pytorch deep-learning based library, might be good if we want to get fancy
  - has implementations out of the box of more novel methods as used in research papers
  - not at all maintained, can't be downloaded by package managers, had to do manually
  - if used we will have to maintain it ourselves, doesn't bode well unless we pool resources into open-source development

- [`tf-recommenders`](https://github.com/tensorflow/recommenders)
  - completely different paradigm to typical sklearn clf.fit type machine learning, so a lot more time to adapt to
  - does seem maintained but is a faff to get setup on m1 at least + cpu/gpu issues, tf2 issues w. outdated docs etc.
  - deep learning based, provides customisability if we want to build our own model architecture (layers and multiple linked models like an autoencoder step etc.)
  - good if we understand tensorflow very well, i will have to learn and am keen to, but i can tell its going to be uphill for a little while at least
  - we can expect google to maintain their libraries or so we hope
  - the interfaces changing from one version to the next, and the troubleshooting of setup requirements and need for extra-engineering effort to get things working is a concern
  - requires a bit more understanding on our part of how to construct and use loss functions and set-up the vector operations in pure tensorflow as well, not as simple as plug and play

**alternative build-our-own libraries**
- `from-scratch in scipy+numpy`
  - would enable implementation of all textbook (linear algebra focussed) methods
  - maximum customisability and knowledge that all dependencies will likely always be met (as long as we use environments to deploy)
  - but would require effort to a) understand what exactly what we want to do, we can't just play and experiment but rather would have to conceptually/theoretically plan and then implement which would improve our understanding of recommender systems and recommender problems but will take time
  - it would also have significant engineering effort in terms of thinking of the software design, classes and interaction of different modules

- `from-scratch in pytorch/tensorflow`
  - would enable implementation of methods used in research papers
  - would require translation of ideas from easier to intuit linear algebra methods to more iterative deep-learning paradigm
  - pytorch might be simpler interface than tensorflow, but both might have quite a few set-up considerations
  - a bit of upskill time to get comfortable

- [`d2l library`](https://d2l.ai/)
  - the textbook provides example of implementing dl-methods, which is its biggest pro
  - it is a wrapper around mxnet which is not a super popular dl-library (but amazon use it)
  - uncertain how likely it is to be maintained (the wrapper)

- `spark-ml` | `fast-ai` | `keras`
- [`recbole`](https://github.com/RUCAIBox/RecBole)
- ...[other-options](https://github.com/topics/sequential-recommendation)
- [implicit](https://github.com/benfred/implicit)
  - implements a lot of


## First Thoughts on Ranking (Very Rough)
*depends on what technique we want to apply and how much customisability we want, but here's my ranking*

1. `surprise`
   1. quickest, simplest, stablest, intuitive and traditional and the creativity can go into the features and problem formulation
3.  `from-scratch in scipy+numpy`
    1.  we know how to do things in these two, we'll get a deeper understanding of what we are doing, but the effort is greater than a ready-built library, but might be too time-consuming
4.  `from-scratch in pytorch/tensorflow`
    1.  would require upskilling, but offers customisability and implementation options from research papers, perhaps session-based recommenders actually require the sequential algorithms from here?
2. `tf-recommenders`
   1. deep learning option, should be stable, hard to learn but follows a pattern at least, likely to be maintained, tricky to setup, might not have intuitive algorithms we are reading in traditional literature so might have to translate notions from there to a more iterative neural network paradigm - but opens up options to implement cool-stuff from research papers
5.  `lightfm`
    1.  confused by interface, but might jump to second place if it turns out to be sensible and have good options, not sure why it would be better than surprise though as seems to only offer subset of traditional techniques with worse interface and smaller user-base
6.  `spotlight`
    1.  such a shame as this was the one i wanted to use, but hasn't been updated in 2 years, issues galore and no responses, not installable from package managers had to do manually, would require us to self-maintain and update, might just be all hassle and no gain, and its benefits could perhaps be gotten from a custom-build and using it as a guide of how to go about it in a pytorch integration setting. but this could still be the dark-horse as it has some out of the box algorithms (which we'd need to understand in depth enough to troubleshoot) from research papers and is built on pytorch thus providing extensibility
7.  ...everything else